package com.stest.maxon.speedtest.ui.fragment.support;


import android.support.v4.app.Fragment;

import com.stest.maxon.speedtest.R;
import com.stest.maxon.speedtest.ui.fragment.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ApproveSendingFragment extends BaseFragment {


    public ApproveSendingFragment() {
        // Required empty public constructor
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_approve_sending;
    }

}
