package com.stest.maxon.speedtest.ui.fragment.support;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.stest.maxon.speedtest.R;
import com.stest.maxon.speedtest.listeners.support.ICommentServiceConsumer;
import com.stest.maxon.speedtest.model.support.Comment;
import com.stest.maxon.speedtest.ui.fragment.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class SendMessageFragment extends BaseFragment {

    private Spinner subjectSpnr;

    private EditText messageEt;

    private LinearLayout sendBtn;

    private ICommentServiceConsumer serviceConsumer;

    public SendMessageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        serviceConsumer = (ICommentServiceConsumer)getContext();

        initViews(view);
        initListeners();
    }

    private void initViews(View view){

        subjectSpnr = (Spinner) view.findViewById(R.id.spinner_subject_type);
        messageEt = (EditText)view.findViewById(R.id.et_message);
        sendBtn = (LinearLayout)view.findViewById(R.id.ll_send_message);

        ArrayAdapter<String> subjectsAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.message_subjects_list));

        subjectSpnr.setAdapter(subjectsAdapter);
        subjectSpnr.setSelection(0);

    }

    private void initListeners(){

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Comment comment = new Comment();
                comment.setSubject((String)subjectSpnr.getSelectedItem());
                comment.setMessage(messageEt.getText().toString());
                comment.setCreatedAt(System.currentTimeMillis());

                serviceConsumer.provideCommentSend(comment);

            }
        });
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_send_message;
    }

}
