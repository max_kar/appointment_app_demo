package com.stest.maxon.speedtest.listeners;

/**
 * Created by Maxon on 8/22/2017.
 */

public interface IServiseConsumer<T, N> {

    void unsubscribe();

}
