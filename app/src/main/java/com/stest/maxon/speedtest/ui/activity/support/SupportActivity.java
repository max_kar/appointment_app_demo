package com.stest.maxon.speedtest.ui.activity.support;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.view.MenuItem;

import com.stest.maxon.speedtest.R;
import com.stest.maxon.speedtest.listeners.support.ICommentServiceConsumer;
import com.stest.maxon.speedtest.model.support.Comment;
import com.stest.maxon.speedtest.service.support.CommentService;
import com.stest.maxon.speedtest.ui.activity.BaseActivity;
import com.stest.maxon.speedtest.ui.adapter.CommentsRVAdapter;
import com.stest.maxon.speedtest.ui.fragment.support.MessagesListFragment;
import com.stest.maxon.speedtest.ui.fragment.support.SendMessageFragment;

public class SupportActivity extends BaseActivity implements ICommentServiceConsumer {

    private static final String TAG = SupportActivity.class.getSimpleName();

    private MessagesListFragment listFragment;

    private SendMessageFragment messageFragment;

    private CommentService commentService;

    private TabLayout allTabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        commentService = new CommentService(this);

        allTabs = (TabLayout) findViewById(R.id.mainTabLayout);
        bindWidgetsWithAnEvent();
        setupTabLayoutComponents();

    }

    /**
     * Initialize child tab layout components
     */
    private void setupTabLayoutComponents() {

        //initialize related fragments instances
        listFragment = new MessagesListFragment();
        messageFragment = new SendMessageFragment();

        //add tabs to tab layout
        allTabs.addTab(allTabs.newTab().setText(R.string.support_fragment_name), true);
        allTabs.addTab(allTabs.newTab().setText(R.string.faq_fragment_name));
    }

    /**
     * Initialize tab layout click event behavior
     */
    private void bindWidgetsWithAnEvent() {
        allTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    /**
     * Replace old fragment with new one
     *
     * @param tabPosition obtained position
     */
    public void setCurrentTabFragment(int tabPosition) {
        switch (tabPosition) {
            case 0:
                replaceFragment(messageFragment);
                break;
            case 1:
                replaceFragment(listFragment);
                break;
            default:
                replaceFragment(messageFragment);
                break;
        }
    }

    /**
     * Replace fragment procedure
     *
     * @param fragment new chosed procedure
     */
    public void replaceFragment(Fragment fragment) {

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.mainViewHolder, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();

    }

    private void initActivityTransition(Class activity) {

        Intent i = new Intent(getBaseContext(), activity);
        startActivity(i);
    }

    @Override
    public void provideCommentSend(Comment comment) {
        commentService.sendComment(comment);
        initActivityTransition(ApprovementActivity.class);
    }

    @Override
    public void provideCommentPagesGetAll(CommentsRVAdapter adapter, boolean init) {
        commentService.readAllCommentPages(adapter, init);
    }

    @Override
    public void provideCommentGetAll(CommentsRVAdapter adapter) {
        commentService.readAllComments(adapter);
    }

    @Override
    public void unsubscribe() {
        commentService.unsubscribe();
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_main;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        this.unsubscribe();
    }
}
