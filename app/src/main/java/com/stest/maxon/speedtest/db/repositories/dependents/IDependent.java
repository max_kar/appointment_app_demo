package com.stest.maxon.speedtest.db.repositories.dependents;

import com.stest.maxon.speedtest.db.IRepository;
import com.stest.maxon.speedtest.model.dependent.Dependent;

import io.reactivex.Observable;

/**
 * Created by Maxon on 8/21/2017.
 */

public interface IDependent extends IRepository<Dependent> {

    @Override
    Observable createEntity(Dependent object);

    @Override
    Observable getAllEntities();
}
