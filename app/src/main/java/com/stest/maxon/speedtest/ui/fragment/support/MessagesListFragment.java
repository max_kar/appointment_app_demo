package com.stest.maxon.speedtest.ui.fragment.support;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.stest.maxon.speedtest.R;
import com.stest.maxon.speedtest.listeners.support.ICommentServiceConsumer;
import com.stest.maxon.speedtest.ui.adapter.CommentsRVAdapter;
import com.stest.maxon.speedtest.ui.fragment.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class MessagesListFragment extends BaseFragment {

    private static final String TAG = MessagesListFragment.class.getSimpleName();

    private RecyclerView commentsList;

    private ICommentServiceConsumer serviceConsumer;

    private CommentsRVAdapter rvAdapter;

    private LinearLayoutManager layoutManager;

    private static int firstVisibleInListview;

    public MessagesListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        serviceConsumer = (ICommentServiceConsumer)getContext();

        initViews(view);

        //serviceConsumer.provideCommentPagesGetAll(rvAdapter, true);
        serviceConsumer.provideCommentGetAll(rvAdapter);
        firstVisibleInListview = layoutManager.findFirstVisibleItemPosition();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initViews(View view){

        commentsList = (RecyclerView)view.findViewById(R.id.comments_recycler_view);
        rvAdapter = new CommentsRVAdapter();
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        commentsList.setLayoutManager(layoutManager);
        commentsList.setAdapter(rvAdapter);

        //commentsList.setOnScrollListener(scrollListener);

    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_messages_list;
    }
    
    private RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int currentFirstVisible = layoutManager.findFirstVisibleItemPosition();

            if(currentFirstVisible > firstVisibleInListview){
                Log.d(TAG, "onScrolled() called with: recyclerView = [" + recyclerView + "], dx = [" + dx + "], dy = [" + dy + "]");
            }else{
                serviceConsumer.provideCommentPagesGetAll(rvAdapter, false);
            }

            firstVisibleInListview = currentFirstVisible;
        }
    };

}
