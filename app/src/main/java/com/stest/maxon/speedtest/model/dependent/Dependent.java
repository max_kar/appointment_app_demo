package com.stest.maxon.speedtest.model.dependent;

import com.stest.maxon.speedtest.model.BaseModel;

/**
 * Created by Maxon on 8/21/2017.
 */

public class Dependent extends BaseModel {

    private String name;

    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
