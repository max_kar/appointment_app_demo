package com.stest.maxon.speedtest.util;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.stest.maxon.speedtest.constants.STConstants;

/**
 * Created by Maxon on 8/21/2017.
 */

public class FirebaseInteractor {

    public static DatabaseReference dbReferenceProvider(){
        return FirebaseDatabase.getInstance().getReferenceFromUrl(STConstants.Database.URL.DB_URL);
    }
}
