package com.stest.maxon.speedtest.model;

/**
 * Created by Maxon on 8/21/2017.
 */

public class Auditable {

    private long createdAt;

    private long updatedAt;

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }
}
