package com.stest.maxon.speedtest.ui.activity.support;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.stest.maxon.speedtest.R;
import com.stest.maxon.speedtest.ui.activity.BaseActivity;
import com.stest.maxon.speedtest.ui.fragment.support.ApproveSendingFragment;

/**
 * Created by Maxon on 8/22/2017.
 */

public class ApprovementActivity extends BaseActivity {

    private ApproveSendingFragment approveSendingFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initViews();
    }

    public void initViews(){
        approveSendingFragment = new ApproveSendingFragment();
        addFragment(approveSendingFragment);
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_approvement;
    }

    public void addFragment(Fragment fragment) {

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(R.id.approvement_fragment_holder, fragment);
        ft.commit();

    }
}
