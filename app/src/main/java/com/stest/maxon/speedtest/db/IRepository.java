package com.stest.maxon.speedtest.db;

import io.reactivex.Observable;

/**
 * Created by Maxon on 8/21/2017.
 */

public interface IRepository<T> {

    Observable createEntity(T object);

    Observable getAllEntities();
}
