package com.stest.maxon.speedtest.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.stest.maxon.speedtest.R;
import com.stest.maxon.speedtest.model.support.Comment;

import java.util.ArrayList;

import dagger.internal.Preconditions;

/**
 * Created by Maxon on 8/22/2017.
 */

public class CommentsRVAdapter extends RecyclerView.Adapter<CommentsRVAdapter.CommentsViewHolder> {

    private ArrayList<Comment> commentsDataSet = new ArrayList<>();

    public CommentsRVAdapter() {
    }

    public CommentsRVAdapter(ArrayList<Comment> commentsDataSet) {
        this.commentsDataSet = Preconditions.checkNotNull(commentsDataSet);
    }

    public ArrayList<Comment> getCommentsDataSet() {
        return commentsDataSet;
    }

    public void setCommentsDataSet(ArrayList<Comment> commentsDataSet) {
        this.commentsDataSet = commentsDataSet;
    }

    public void expandDataSet(ArrayList<Comment> newDataSet){
        Preconditions.checkNotNull(commentsDataSet).addAll(newDataSet);
    }

    @Override
    public CommentsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comment_card, parent, false);

        CommentsViewHolder myViewHolder = new CommentsViewHolder(view);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(CommentsViewHolder holder, int position) {

        Comment comment = commentsDataSet.get(position);

        holder.tvSubject.setText(comment.getSubject());
        holder.tvDescription.setText(comment.getMessage());

    }

    public static class CommentsViewHolder extends RecyclerView.ViewHolder {

        TextView tvSubject;

        TextView tvDescription;

        public CommentsViewHolder(View itemView) {
            super(itemView);

            this.tvSubject = (TextView) itemView.findViewById(R.id.tv_subject_text);
            this.tvDescription = (TextView) itemView.findViewById(R.id.tv_description_text);
        }
    }

    @Override
    public int getItemCount() {
        return  Preconditions.checkNotNull(commentsDataSet).size();
    }

}
