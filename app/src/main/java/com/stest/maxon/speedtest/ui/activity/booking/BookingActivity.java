package com.stest.maxon.speedtest.ui.activity.booking;

import android.os.Bundle;
import android.widget.TextView;

import com.stest.maxon.speedtest.R;
import com.stest.maxon.speedtest.ui.activity.BaseActivity;

/**
 * Created by Maxon on 8/23/2017.
 */

public class BookingActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initViews();
    }

    public void initViews(){
        TextView textView = (TextView)findViewById(R.id.tv_title_booking);
        textView.setText(this.getClass().getCanonicalName());
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_common_booking;
    }
}
