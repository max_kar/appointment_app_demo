package com.stest.maxon.speedtest.service.support;

import com.stest.maxon.speedtest.model.support.Comment;
import com.stest.maxon.speedtest.service.IService;
import com.stest.maxon.speedtest.ui.adapter.CommentsRVAdapter;

/**
 * Created by Maxon on 8/21/2017.
 */

public interface ICommentService extends IService<Comment, CommentsRVAdapter>{

    void sendComment(Comment comment);

    void readAllCommentPages(CommentsRVAdapter adapter, Boolean initialRequest);

    void readAllComments(CommentsRVAdapter adapter);
}
