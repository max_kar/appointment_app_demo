package com.stest.maxon.speedtest.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.stest.maxon.speedtest.R;
import com.stest.maxon.speedtest.ui.activity.authorization.LoginActivity;
import com.stest.maxon.speedtest.ui.activity.booking.BookingActivity;
import com.stest.maxon.speedtest.ui.activity.consultation.ConsultationActivity;
import com.stest.maxon.speedtest.ui.activity.dependants.DependentActivity;
import com.stest.maxon.speedtest.ui.activity.settings.SettingsActivity;
import com.stest.maxon.speedtest.ui.activity.support.SupportActivity;


public class StartActivity extends BaseActivity {

    private static final String TAG = StartActivity.class.getSimpleName();

    private RelativeLayout bookingLayout, loginLayout, settingsLayout, dependentsLayout, consultationLayout, supportLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupLayout();
        setupListeners();
    }

    private void intentSender(Class activity) {

        Intent i = new Intent(getBaseContext(), activity);
        startActivity(i);
    }

    private void setupListeners() {

        bookingLayout.setOnClickListener(performAction(BookingActivity.class));
        loginLayout.setOnClickListener(performAction(LoginActivity.class));
        settingsLayout.setOnClickListener(performAction(SettingsActivity.class));
        dependentsLayout.setOnClickListener(performAction(DependentActivity.class));
        consultationLayout.setOnClickListener(performAction(ConsultationActivity.class));
        supportLayout.setOnClickListener(performAction(SupportActivity.class));
    }

    private View.OnClickListener performAction(final Class activity) {

        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentSender(activity);
            }
        };
    }

    private void setupLayout() {

        bookingLayout = (RelativeLayout) findViewById(R.id.mm_item_booking);
        loginLayout = (RelativeLayout) findViewById(R.id.mm_item_login);
        settingsLayout = (RelativeLayout) findViewById(R.id.mm_item_settings);
        dependentsLayout = (RelativeLayout) findViewById(R.id.mm_item_dependents);
        consultationLayout = (RelativeLayout) findViewById(R.id.mm_item_consultation);
        supportLayout = (RelativeLayout) findViewById(R.id.mm_item_support);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        finish();
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_start;
    }
}
