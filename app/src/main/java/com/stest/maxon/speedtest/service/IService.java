package com.stest.maxon.speedtest.service;

/**
 * Created by Maxon on 8/21/2017.
 */

public interface IService<T, N> {

    public void unsubscribe();
}
