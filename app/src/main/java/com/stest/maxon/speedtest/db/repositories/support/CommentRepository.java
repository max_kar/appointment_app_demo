package com.stest.maxon.speedtest.db.repositories.support;


import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.stest.maxon.speedtest.constants.STConstants;
import com.stest.maxon.speedtest.model.support.Comment;

import dagger.internal.Preconditions;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

/**
 * Created by Maxon on 8/21/2017.
 */

public class CommentRepository implements IComment {

    private DatabaseReference databaseReference;

    public CommentRepository(DatabaseReference databaseReference) {
        this.databaseReference = Preconditions.checkNotNull(databaseReference);
    }

    @Override
    public Observable<Boolean> createEntity(final Comment object) {
        return Observable.create(new ObservableOnSubscribe<Boolean>() {
            @Override
            public void subscribe(@io.reactivex.annotations.NonNull final ObservableEmitter e) throws Exception {
                databaseReference.child(STConstants.Database.Names.TABLE_COMMENTS)
                        .push()
                        .setValue(object)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                e.onNext(task.isSuccessful());
                                e.onComplete();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception ex) {
                                e.onError(new FirebaseException(ex.getMessage()));
                            }
                        });
            }
        });
    }

    @Override
    public Observable getAllEntities() {

        Query query = databaseReference
                .child(STConstants.Database.Names.TABLE_COMMENTS)
                .orderByChild("createdAt");

        return observe(query);
    }

    public Observable getEntities(long time) {

        Query query = databaseReference
                .child(STConstants.Database.Names.TABLE_COMMENTS)
                .orderByChild("createdAt")
                .endAt(new Long(time).doubleValue())
                .limitToLast(1);

        return observe(query);
    }
    public Observable getInitialEntities() {

        Query query = databaseReference
                .child(STConstants.Database.Names.TABLE_COMMENTS)
                .orderByKey()
                .limitToLast(5);

        return observe(query);
    }

    private Observable<DataSnapshot> observe(final Query ref) {
        return Observable.create(new ObservableOnSubscribe<DataSnapshot>() {
            @Override
            public void subscribe(@io.reactivex.annotations.NonNull final ObservableEmitter<DataSnapshot> e) throws Exception {

                ref.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        e.onNext(dataSnapshot);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        e.onError(new FirebaseException(databaseError.getMessage()));
                    }
                });
            }
        });
    }
}
