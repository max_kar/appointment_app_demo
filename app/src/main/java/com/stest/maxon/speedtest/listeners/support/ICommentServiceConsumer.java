package com.stest.maxon.speedtest.listeners.support;

import com.stest.maxon.speedtest.listeners.IServiseConsumer;
import com.stest.maxon.speedtest.model.support.Comment;
import com.stest.maxon.speedtest.ui.adapter.CommentsRVAdapter;

/**
 * Created by Maxon on 8/22/2017.
 */

public interface ICommentServiceConsumer extends IServiseConsumer<Comment, CommentsRVAdapter> {

    void provideCommentSend(Comment comment);

    void provideCommentPagesGetAll(CommentsRVAdapter adapter, boolean init);

    void provideCommentGetAll(CommentsRVAdapter adapter);

}
