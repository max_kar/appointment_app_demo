package com.stest.maxon.speedtest.constants;

/**
 * Created by Maxon on 8/21/2017.
 */

public class STConstants {

    public static class Database {

        public static class URL{

            public static final String DB_URL = "https://speedtest-fc8af.firebaseio.com/";

        }

        public static class Names {

            public static final String TABLE_COMMENTS = "comments";
        }
    }
}
