package com.stest.maxon.speedtest.db.repositories.support;

import com.stest.maxon.speedtest.db.IRepository;
import com.stest.maxon.speedtest.model.support.Comment;

import io.reactivex.Observable;

/**
 * Created by Maxon on 8/21/2017.
 */

public interface IComment extends IRepository<Comment>{

    @Override
    Observable createEntity(Comment object);

    @Override
    Observable getAllEntities();
}
