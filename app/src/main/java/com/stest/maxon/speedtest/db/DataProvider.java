package com.stest.maxon.speedtest.db;

import android.content.Context;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.stest.maxon.speedtest.db.repositories.support.CommentRepository;
import com.stest.maxon.speedtest.db.repositories.support.IComment;
import com.stest.maxon.speedtest.model.support.Comment;
import com.stest.maxon.speedtest.util.FirebaseInteractor;

import io.reactivex.Observable;

/**
 * Created by Maxon on 8/21/2017.
 */

public class DataProvider {

    private Context context;

    public DataProvider(Context context) {
        this.context = context;
    }

    public IComment provideComments() {
        int playServicesStatus =
                GoogleApiAvailability.getInstance()
                        .isGooglePlayServicesAvailable(context);

        if (playServicesStatus == ConnectionResult.SUCCESS) {
            return new CommentRepository(FirebaseInteractor.dbReferenceProvider());
        } else {
            return new IComment() {
                @Override
                public Observable createEntity(Comment object) {
                    return Observable.error(
                            new RuntimeException(
                                    "FireBase not supported"));
                }

                @Override
                public Observable getAllEntities() {
                    return Observable.error(
                            new RuntimeException(
                                    "FireBase not supported"));
                }

            };
        }
    }

}
