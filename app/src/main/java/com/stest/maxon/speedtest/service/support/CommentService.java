package com.stest.maxon.speedtest.service.support;

import android.content.Context;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.stest.maxon.speedtest.db.DataProvider;
import com.stest.maxon.speedtest.db.repositories.support.CommentRepository;
import com.stest.maxon.speedtest.model.support.Comment;
import com.stest.maxon.speedtest.ui.adapter.CommentsRVAdapter;

import java.util.ArrayList;
import java.util.Collections;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Maxon on 8/21/2017.
 */

public class CommentService implements ICommentService {

    private static final String TAG = CommentService.class.getSimpleName();

    private CommentRepository commentRepository;

    private Disposable disposableSM;

    private Disposable disposableRM;

    private static long timestamp;

    public CommentService(Context context) {
        DataProvider dataProvider = new DataProvider(context);
        this.commentRepository = (CommentRepository) dataProvider.provideComments();
    }

    @Override
    public void sendComment(Comment comment) {

        disposableSM = commentRepository.createEntity(comment).observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe(new Consumer<Boolean>() {
            @Override
            public void accept(@NonNull Boolean aBoolean) throws Exception {

            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(@NonNull Throwable throwable) throws Exception {
                Log.d(TAG, "accept() called with: throwable = [" + throwable + "]");
            }
        }, new Action() {
            @Override
            public void run() throws Exception {
                Log.d(TAG, "run() called");
            }
        });

    }

    @Override
    public void readAllCommentPages(final CommentsRVAdapter adapter, Boolean initialRequest) {

        if (initialRequest) {
            disposableRM = commentRepository.getInitialEntities()
                    .observeOn(AndroidSchedulers.mainThread()).
                    subscribeOn(Schedulers.io()).subscribe(
                            provideOnNextPagesConsumer(adapter),
                            provideOnErrorConsumer(),
                            provideOnCompleteConsumer());
        } else {
            disposableRM = commentRepository.getEntities(timestamp)
                    .observeOn(AndroidSchedulers.mainThread()).
                    subscribeOn(Schedulers.io()).subscribe(
                            provideOnNextPagesConsumer(adapter),
                            provideOnErrorConsumer(),
                            provideOnCompleteConsumer());
        }
    }
    @Override
    public void readAllComments(final CommentsRVAdapter adapter) {

            disposableRM = commentRepository.getAllEntities()
                    .observeOn(AndroidSchedulers.mainThread()).
                    subscribeOn(Schedulers.io()).subscribe(
                            provideOnNextConsumer(adapter),
                            provideOnErrorConsumer(),
                            provideOnCompleteConsumer());

    }

    @Override
    public void unsubscribe() {

        if (disposableRM != null && !disposableRM.isDisposed()) {
            disposableRM.dispose();
        }

        if (disposableSM != null && !disposableSM.isDisposed()) {
            disposableSM.dispose();
        }
    }

    private Consumer<DataSnapshot> provideOnNextPagesConsumer(final CommentsRVAdapter adapter) {
        return new Consumer<DataSnapshot>() {
            @Override
            public void accept(@NonNull DataSnapshot snapshot) throws Exception {

                ArrayList<Comment> comments = new ArrayList<>();

                for (DataSnapshot sensorModelSnapshot : snapshot.getChildren()) {
                    Comment comment = sensorModelSnapshot.getValue(Comment.class);
                    comments.add(comment);
                }
                Collections.reverse(comments);
                timestamp = comments.get(comments.size() - 1).getCreatedAt();
                adapter.expandDataSet(new ArrayList<Comment>(comments.subList(0, comments.size() - 1)));
                adapter.notifyDataSetChanged();
            }
        };
    }

    private Consumer<DataSnapshot> provideOnNextConsumer(final CommentsRVAdapter adapter) {
        return new Consumer<DataSnapshot>() {
            @Override
            public void accept(@NonNull DataSnapshot snapshot) throws Exception {

                ArrayList<Comment> comments = new ArrayList<>();

                for (DataSnapshot sensorModelSnapshot : snapshot.getChildren()) {
                    Comment comment = sensorModelSnapshot.getValue(Comment.class);
                    comments.add(comment);
                }
                Collections.reverse(comments);

                adapter.expandDataSet(comments);
                adapter.notifyDataSetChanged();
            }
        };
    }

    private Consumer<Throwable> provideOnErrorConsumer() {
        return new Consumer<Throwable>() {
            @Override
            public void accept(@NonNull Throwable throwable) throws Exception {
                Log.d(TAG, "accept() called with: throwable = [" + throwable + "]");
            }
        };
    }

    private Action provideOnCompleteConsumer() {
        return new Action() {
            @Override
            public void run() throws Exception {
                Log.d(TAG, "run() called");
            }
        };
    }
}
