package com.stest.maxon.speedtest.app;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

/**
 * Created by Maxon on 8/20/2017.
 */

public class STApplication extends Application {

    private PackageManager packManager;

    private static STApplication stApplication;

    private static Context context;

    public static STApplication getInstance() {
        return stApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        stApplication = this;
        context = this;
        packManager = this.getPackageManager();
    }

    public static Context getContext() {
        return context;
    }

    public String getAppVersion() {

        PackageInfo info = null;
        try {
            info = packManager.getPackageInfo(
                    this.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = info.versionName;

        return version;
    }
}
