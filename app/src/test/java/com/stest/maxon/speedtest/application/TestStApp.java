package com.stest.maxon.speedtest.application;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.stest.maxon.speedtest.app.STApplication;

/**
 * Created by Maxon on 8/23/2017.
 */

public class TestStApp extends STApplication {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        context = this;
    }

    public static Context getContext() {
        return context;
    }

}
