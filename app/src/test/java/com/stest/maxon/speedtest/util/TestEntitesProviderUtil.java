package com.stest.maxon.speedtest.util;

import com.stest.maxon.speedtest.model.support.Comment;

/**
 * Utility class that provides database used entities with predefined values
 */

public final class TestEntitesProviderUtil {

    /**
     * Comment entity providing method
     *
     * @return new Comment object
     */
    public Comment getComment() {

        Comment comment = new Comment();
        comment.setSubject(String.valueOf(Comment.getGUID()));
        comment.setMessage("This is test message");
        comment.setCreatedAt(System.currentTimeMillis());

        return comment;
    }

}
