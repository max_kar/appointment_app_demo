package com.stest.maxon.speedtest.common;

import android.content.Context;
import android.test.mock.MockContext;

import com.stest.maxon.speedtest.application.TestStApp;
import com.stest.maxon.speedtest.util.TestEntitesProviderUtil;

import org.junit.Before;

/**
 * Created by Maxon on 8/23/2017.
 */

public class BaseTest {

    protected Context context;

    protected TestEntitesProviderUtil entitesProviderUtil;

    public void setContext(Context context) {
        this.context = new MockContext();
    }

    /**
     * Pre-execution testsuite procedure
     */
    @Before
    public void setUp() {
        setContext(TestStApp.getContext());
        entitesProviderUtil = new TestEntitesProviderUtil();
    }
}
