package com.stest.maxon.speedtest.db.repositories.support;

import com.stest.maxon.speedtest.model.support.Comment;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Maxon on 8/23/2017.
 */
public class CommentRepositoryTest extends BaseDBTest {

    private Comment comment;

    @Before
    @Override
    public void setUp() {
        super.setUp();

        comment = entitesProviderUtil.getComment();
    }

    @Test
    public void createEntity() throws Exception {
        commentService.sendComment(comment);
    }

    @Test
    public void getAllEntities() throws Exception {
        commentService.readAllComments(commentsRVAdapter);
    }

    @Test
    public void getEntities() throws Exception {
        commentService.readAllCommentPages(commentsRVAdapter, false);
    }

    @Test
    public void getInitialEntities() throws Exception {
        commentService.readAllCommentPages(commentsRVAdapter, true);
    }

}