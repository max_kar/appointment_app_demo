package com.stest.maxon.speedtest.db.repositories.support;

import com.stest.maxon.speedtest.common.BaseTest;
import com.stest.maxon.speedtest.service.support.CommentService;
import com.stest.maxon.speedtest.ui.adapter.CommentsRVAdapter;

import org.junit.Before;
import org.mockito.Mockito;

/**
 * Created by Maxon on 8/23/2017.
 */

public class BaseDBTest extends BaseTest{


    // CommentRepository testing object
    protected CommentRepository commentRepository;

    // CommentService testing object
    protected CommentService commentService;

    // CommentService testing object
    protected CommentsRVAdapter commentsRVAdapter;


    /**
     * Pre-execution testsuite procedure
     */
    @Before
    @Override
    public void setUp() {
        super.setUp();

        commentRepository = Mockito.mock(CommentRepository.class);
        commentService = Mockito.mock(CommentService.class);
        commentsRVAdapter = Mockito.mock(CommentsRVAdapter.class);

    }
}
