package com.stest.maxon.speedtest.firebase;

import com.stest.maxon.speedtest.common.BaseTest;
import com.stest.maxon.speedtest.util.FirebaseInteractor;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.Assert.*;

/**
 * Created by Maxon on 8/23/2017.
 */
public class FirebaseInteractorTest extends BaseTest {

    private FirebaseInteractor firebaseInteractor;

    @Before
    public void setUp() {
    }

    @Ignore
    @Test
    public void dbReferenceProvider() throws Exception {
        assertNotNull(FirebaseInteractor.dbReferenceProvider());
    }

    @After
    public void tearDown() throws Exception {

    }

}